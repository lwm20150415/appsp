package com.anji.sp.service.impl;


import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by raodeming on 2021/8/16.
 */
public class MyAuthenticationProviderTest {

    @Test
    public void pass(){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");
        boolean matches = bCryptPasswordEncoder.matches("123456", encode);

        System.out.println(encode);
        System.out.println(matches);

    }

}
