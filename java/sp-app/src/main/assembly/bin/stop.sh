#!/bin/bash
pid=`ps ax | grep -i 'sp-app' | grep java | grep -v grep | awk '{print $1}'`
if [ -z "$pid" ] ; then
        echo "No sp-app Server running."
        exit -1;
fi

kill -9 ${pid}

echo "Send shutdown request to sp-app(${pid}) OK"
