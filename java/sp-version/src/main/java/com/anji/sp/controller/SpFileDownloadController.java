package com.anji.sp.controller;

import com.anji.sp.model.ResponseModel;
import com.anji.sp.model.vo.SpUploadVO;
import com.anji.sp.service.SpDownloadService;
import com.anji.sp.service.SpUploadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 下载
 *
 * @author Raod
 * @date 2021/08/20
 */
@RestController
@RequestMapping
@Api(tags = "下载功能")
public class SpFileDownloadController {

    @Autowired
    private SpDownloadService spDownloadService;

    /**
     * 文件上传
     * @param spUploadVO
     * @return
     */

    /**
     * 文件下载
     *
     * @param sdk     appId
     *                sdktest102-1629448268933-2-b6af895c0d1146d5a10d4367a664721e.apk
     * @return
     */
    @ApiOperation(value = "文件下载", notes = "文件下载")
    @GetMapping(value = "/download/{sdk}")
    public ResponseEntity<byte[]> uploadFile(HttpServletRequest request, HttpServletResponse response, @PathVariable("sdk") String sdk) {
        return spDownloadService.download(request, response, sdk);
    }
}

