package com.anji.sp.push.controller;

import com.anji.sp.aspect.PreSpAuthorize;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.vo.PushUserVO;
import com.anji.sp.push.model.vo.RequestSendBean;
import com.anji.sp.push.service.impl.AJPushSendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Kean
 * @Date: 2021/1/20
 * @Description:
 */
@RestController
@RequestMapping("/pushWeb")
@Api(tags = "推送内部调用")
public class PushWebController {

    @Autowired
    private AJPushSendService ajPushSendService;

    @ApiOperation(value = "根据appKey批量推送", httpMethod = "POST")
    @RequestMapping("/pushBatch")
    @PreSpAuthorize("system:user:push")
    public ResponseModel pushBatch(@RequestBody RequestSendBean requestSendBean) {
        requestSendBean.setSendOrigin("0");
        return ajPushSendService.pushBatch(requestSendBean);
    }
    
    @ApiOperation(value = "根据appKey全部推送", httpMethod = "POST")
    @RequestMapping("/pushAll")
    @PreSpAuthorize("system:user:push")
    public ResponseModel pushAll(@RequestBody RequestSendBean requestSendBean) {
        requestSendBean.setSendOrigin("0");
        return ajPushSendService.pushAll(requestSendBean);
    }
}
