//
//  AJPushService.h
//  AJPushSDK
//
//  Created by Black on 2021/2/2.
//  Copyright © 2018 anji-plus 安吉加加信息技术有限公司 http://www.anji-plus.com. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

extern NSString *const kAJPushServiceCustomMessageNotification; //自定义消息
extern NSString *const kAJPushServiceNoticeBarMessageNotification; //通知栏消息
extern NSString *const kAJPushServiceInnerMessageNotification; //内部消息提醒

@interface AJPushService : NSObject
@property (nonatomic,copy) NSString *registrationID;

/*!
 * @abstract 创建AJPush服务单例
 */
+ (instancetype)shareService;
/*!
 * @abstract 启动极光推送JPushSDK
 *
 * @param launchingOption 启动参数.
 * @param appKey 一个JPush 应用必须的,唯一的标识. 请参考 JPush 相关说明文档来获取这个标识.
 * @param ajAppKey 移动服务平台应用key.
 * @param channel 发布渠道. 可选.
 * @param isProduction 是否生产环境. 如果为开发状态,设置为 NO; 如果为生产状态,应改为 YES.
 *                     App 证书环境取决于profile provision的配置，此处建议与证书环境保持一致. *
 * @discussion 提供SDK启动必须的参数, 来启动 SDK.
 * 此接口必须在 App 启动时调用, 否则 JPush SDK 将无法正常工作.
 * */
- (void)registerJPushOption:(NSDictionary *)launchingOption
                 appKey:(NSString *)appKey
                ajAppKey:(NSString *)ajAppKey
                channel:(NSString *)channel
       apsForProduction:(BOOL)isProduction;

/*!
 * @abstract 获取deviceToKen后注册推送服务
 *
 * @param deviceToken  APNS返回推送令牌
 * @param baseUrl 支持外部配置请求服务eg：http://appsp.test.anji-plus.com  默认传nil即可
 * 此接口必须在 App didRegisterForRemoteNotificationsWithDeviceToken后调用
 * */
- (void)registerDeviceToken:(NSData *)deviceToken withUrl:(nullable NSString *)baseUrl;
//判断环境 true 生产；false 测试
- (BOOL)isProduction;

@end

NS_ASSUME_NONNULL_END
