# 对外接口
域名：https://openappsp.anji-plus.com

## APP初始化
App推送初始化，通过SDK获取deviceId保存后端。   

```建议：集成方把deviceId和用户token关联入库，方便定向推送```

**接口**    
`/sp/push/init`

**请求类型**    
post请求

**请求参数**    
``` json
{
    "appKey": "fae48dd5c3834ff4aac9a12e79b0b481", 
    "deviceId": "8021f52a9e705cc574:60:fa:f7:57:fd", 
    "manuToken": "IQAAAACy0cz8AACoKEK_GnnBME8DfkidrV5x4Tg3mMQdO9FWdP49jEClbMvPZyLBdsp8T9z0c6L6WBrDfTvqF2yba5ULlsVaV3NB0NdLf18PV9_3_g", 
    "registrationId":"120c83f7609c0ec3495", 
    "deviceType": "1", 
    "brand":"POT-AL00a", 
    "osVersion":"10" 
}
```

***

| 字段| 类型| 说明 | 是否必传 |
| :-----| :---- | :---- | :---- |
| appKey |string | 应用唯一key | 必传 |
| deviceId |string | 设备唯一标识 | 必传 |
| manuToken | string | 厂商通道的token或者regId(华为、oppo、vivo、小米传值) | 四大厂商必传 |
| registrationId | string | 极光的用户id | 必传 |
| deviceType | string | 设备类型：0:其他手机，1:华为，2:小米，3:oppo，4:vivo，5：ios | 必传 |
| brand | string | 设备品牌（例如：小米6，iPhone 7plus） | 必传 |
| osVersion | string | 系统版本例如：7.1.2  13.4.1 | 必传 |



**请求回调**    
``` json
{
 	"repCode": "0000", 
 	"repMsg": "成功", 
 	"repData": {}
 }
```
***

| 字段| 类型| 说明 |
| :-----| :---- | :---- |
| repCode |string | 业务返回码，0000表示成功  |
| repMsg |string | 业务日志、异常信息 |
| repData | Object | 请求业务数据包、详情见下 |


## 批量推送
后端服务批量推送消息至AJPlus服务器，由AJPush服务器回调消息的MsgId,供后端服务查询推送详细结果    
**注意：** `一次批量推送deviceIds最大支持1000条`

**接口**    
`/sp/push/pushBatch`

**请求类型**    
post请求

**请求参数**    
``` json
{
	"appKey": "fae48dd5c3834ff4aac9a12e79b0b481",
	"secretKey": "55c773aee67245189a4dc98d5a46c1ab",
	"title":"第三方调用测试333323",
	"content":"推送内容",
	"pushType":"0",
	"deviceIds":[
		"13165ffa4ea9462aecf"],
	"extras":{
		"aaaa":"axxxx",
		"cccc":"vxxx"
	},
	"androidConfig":{
		"sound":"xxx"
	},
	"iosConfig":{
		"sound":"xxx.caf"
	}
}
```

***

| 字段| 类型| 说明 | 是否必传 |
| :-----| :---- | :---- | :---- |
| appKey |String | 应用唯一key | 必传 |
| secretKey |String | 应用秘钥 | 必传 |
| title | String | 推送标题 | 必传 |
| content | String | 推送内容 | 必传 |
| pushType | String | 推送类型 1透传消息 0 普通消息（默认0） | 可选 |
| deviceIds | List | 设备id列表（最大1000条） | 必传 |
| extras | Map<String,String> | 推送透传消息内容 | 可选 |
| androidConfig | Map<String,Object> | Android其他配置例如：传声音{"sound":"xxx"} | 可选 |
| iosConfig | Map<String,Object> | iOS其他配置例如：传声音{"sound":"xxx.caf"} | 可选 |



**请求回调**    
``` json
{
    "repCode": "0000",
    "repMsg": "成功",
    "repData": {
        "msgId": "813813041454538752",
        "appKey": "fae48dd5c3834ff4aac9a12e79b0b481"
    },
    "success": true,
    "error": false
}
```
***

| 字段| 类型| 说明 |
| :-----| :---- | :---- |
| repCode |String | 业务返回码，0000表示成功  |
| repMsg |String | 业务日志、异常信息 |
| repData | Object | 请求业务数据包、详情见下 |
| ------- | ---- | ------ |
| msgId | String | 消息唯一id,查询消息推送结果使用 |
| appKey | String | 应用唯一key |

## 全部推送
一次性推送对应APP全部用户    
**注意：** `一次推送将触发对应平台所有设备，请谨慎使用`

**接口**    
`/sp/push/pushAll`

**请求类型**    
post请求

**请求参数**    
``` json
{
	"appKey": "fae48dd5c3834ff4aac9a12e79b0b481",
	"secretKey": "55c773aee67245189a4dc98d5a46c1ab",
	"title":"第三方调用测试333323",
	"content":"推送内容",
	"pushType":"0",
	"extras":{
		"aaaa":"axxxx",
		"cccc":"vxxx"
	},
	"androidConfig":{
		"sound":"xxx"
	},
	"iosConfig":{
		"sound":"xxx.caf"
	}
}
```

***

| 字段| 类型| 说明 | 是否必传 |
| :-----| :---- | :---- | :---- |
| appKey |String | 应用唯一key | 必传 |
| secretKey |String | 应用秘钥 | 必传 |
| title | String | 推送标题 | 必传 |
| content | String | 推送内容 | 必传 |
| pushType | String | 推送类型 1透传消息 0 普通消息（默认0） | 可选 |
| extras | Map<String,String> | 推送透传消息内容 | 可选 |
| androidConfig | Map<String,Object> | Android其他配置例如：传声音{"sound":"xxx"} | 可选 |
| iosConfig | Map<String,Object> | iOS其他配置例如：传声音{"sound":"xxx.caf"} | 可选 |



**请求回调**    
``` json
{
    "repCode": "0000",
    "repMsg": "成功",
    "repData": {
        "msgId": "813813041454538752",
        "appKey": "fae48dd5c3834ff4aac9a12e79b0b481"
    },
    "success": true,
    "error": false
}
```
***

| 字段| 类型| 说明 |
| :-----| :---- | :---- |
| repCode |String | 业务返回码，0000表示成功  |
| repMsg |String | 业务日志、异常信息 |
| repData | Object | 请求业务数据包、详情见下 |
| ------- | ---- | ------ |
| msgId | String | 消息唯一id,查询消息推送结果使用 |
| appKey | String | 应用唯一key |


## 消息历史
根据msgId和appKey查询消息历史   
**注意：** `单条消息所有推送详细结果，包含极光Android和iOS、小米、华为、OPPO、vivo`

**接口**    
`/sp/push/queryHistoryByAppKeyAndMsgId`

**请求类型**    
post请求

**请求参数**    
``` json
{
	"appKey": "fae48dd5c3834ff4aac9a12e79b0b481",
	"msgId": "806552772384567296"
}
```

***

| 字段| 类型| 说明 | 是否必传 |
| :-----| :---- | :---- | :---- |
| appKey |String | 应用唯一key | 必传 |
| msgId |String | 推送返回的消息id | 必传 |

**请求回调**    
``` json
{
    "repCode": "0000",
    "repMsg": "成功",
    "repData": {
        "appKey": "fae48dd5c3834ff4aac9a12e79b0b481",
        "msgId": "806552772384567296",
        "targetNum": "12",
        "successNum": "9",
        "title": "妹子测试",
        "content": "妹子测试",
        "extras": null,
        "iosConfig": "{}",
        "androidConfig": "{}",
        "consumptionState": 1,
        "pushType": "0",
        "details": [
            {
                "targetName": "华为",
                "targetNum": "1",
                "successNum": "1"
            },
            {
                "targetName": "小米",
                "targetNum": "2",
                "successNum": "2"
            },
            {
                "targetName": "oppo",
                "targetNum": "2",
                "successNum": "2"
            },
            {
                "targetName": "极光Android",
                "targetNum": "2",
                "successNum": "0"
            },
            {
                "targetName": "vivo",
                "targetNum": "1",
                "successNum": "1"
            },
            {
                "targetName": "vivo",
                "targetNum": "1",
                "successNum": "0"
            },
            {
                "targetName": "极光iOS",
                "targetNum": "3",
                "successNum": "3"
            }
        ]
    },
    "success": true,
    "error": false
}
```
***

| 字段| 类型| 说明 |
| :-----| :---- | :---- |
| repCode |String | 业务返回码，0000表示成功  |
| repMsg |String | 业务日志、异常信息 |
| repData | Object | 请求业务数据包、详情见下 |
| msgId| String | 消息唯一id,查询消息推送结果使用 |
| appKey  | String | 应用唯一key |
| targetNum | String | 总目标数目 |
| successNum | String | 总成功数目 |
| title | String | 标题 |
| content | String | 内容 |
| extras | String | 传递参数 |
| iosConfig | String | iOS配置（声音） |
| androidConfig | String | Android配置（声音） |
| consumptionState | String | 消息状态（0未消费、1已消费） |
| pushType | String | 推送类型 1透传消息 0 普通消息 |
| details | List | 消息详情 |
| ------- | ---- | ------ |
| targetName | String | 平台名称 |
| targetNum | String | 平台目标数|
| successNum | String | 平台成功数 |
