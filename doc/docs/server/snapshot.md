# 快速了解

## 项目介绍
AppSp是一款专为APP打造的全栈式移动服务平台，目前有版本管理、推送管理、公告管理功能。

* AppSp仓库地址: [https://gitee.com/anji-plus/appsp](https://gitee.com/anji-plus/appsp)
* Git下载地址：[https://gitee.com/anji-plus/appsp.git](https://gitee.com/anji-plus/appsp.git)

各端代码目录：
* Java后端: /java
* Web: /web
* Flutter SDK:  /sdk/futter
* Flutter Demo: /demo/futter
* Android SDK: /sdk/android
* Android Demo: /demo/android
* iOS SDK: /sdk/ios
* iOS Demo: /demo/ios

## 主要特性
- 完善的服务流程
- 原生，flutter跨平台支持
- Maven多模块依赖管理
- SDK  Cocopods、Gradle 组件版本管理

## 技术选项
#### 1、系统环境
- Java EE 8
- Servlet 3.0
- 前端：VS Code
- iOS：Xcode
- Android：Android Studio

#### 2、主框架
- Spring Boot
- Spring Framework
- Vue
- Flutter

#### 3、持久层
- Apache MyBatis 3.3
- Alibaba Druid 1.1

#### 3、视图层
- Vue 2.6
- Axios 0.18
- Element UI 2.11

## 内置功能
* 应用管理：可以新增、删除、修改应用信息
    * 版本管理：核心功能, 对版本信息进行CRUD,版本更新主要分为历史版本更新、系统版本更新及灰度发布更新。
	* 推送管理：核心功能，以极光推送兜底，整合华为、小米、oppo、vivo厂商通道，即使应用进程杀死也能收到消息，提高消息抵达率， 
			    可进行推送测试、透传测试，可查看推送历史，。
    * 公告管理：公告管理是通过设置公告的持续时间及对应的公共模板，为APP分发不同的公共。
    * 成员管理：针对应用单独添加用户进入应用群组，还可单独配置每个用户管理该应用的版本、公告等权限信息。
* 账号管理：添加用户，为整个服务平台创建用户
* 文档集成：查看服务平台所有使用方法
* 基础设置：包含系统（iOS、Android）版本基础配置、公告模型基础配置、角色权限分配
